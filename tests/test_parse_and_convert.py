from gomoddepviz import parse_gomod, GoRef, gomod_to_node

NO_DEPENDENCY_GOMOD = """
module gitlab.com/pierredittgen/nestor

go 1.17
"""

SIMPLE_DEPENDENCY_GOMOD = """
module github.com/gophercises/urlshort

go 1.17

require gopkg.in/yaml.v2 v2.4.0 // indirect
"""

MULTIPLE_DEPENDENCIES_GOMOD = """
module example/hello

go 1.17

require rsc.io/quote v1.5.2

require (
	golang.org/x/text v0.0.0-20170915032832-14c0d48ead0c // indirect
	rsc.io/sampler v1.3.0 // indirect
)
"""


def test_parse_no_dependency():
    gomod = parse_gomod(NO_DEPENDENCY_GOMOD)
    assert gomod.module_name == "gitlab.com/pierredittgen/nestor"
    assert gomod.go_version == "1.17"
    assert gomod.dependencies == []


def test_convert_no_dependency():
    gomod = parse_gomod(NO_DEPENDENCY_GOMOD)
    node = gomod_to_node(gomod)
    assert node.name == gomod.module_name
    assert node.dependencies == []


def check_dependency(goref: GoRef, name: str, version: str, comment: str):
    assert goref.ref_name == name
    assert goref.ref_version == version
    assert goref.ref_comment == comment


def test_parse_simple_dependency():
    gomod = parse_gomod(SIMPLE_DEPENDENCY_GOMOD)
    assert gomod.module_name == "github.com/gophercises/urlshort"
    assert gomod.go_version == "1.17"
    assert len(gomod.dependencies) == 1
    dep = gomod.dependencies[0]
    check_dependency(dep, "gopkg.in/yaml.v2", "v2.4.0", "indirect")


def test_convert_simple_dependency():
    gomod = parse_gomod(SIMPLE_DEPENDENCY_GOMOD)
    node = gomod_to_node(gomod)
    assert node.name == gomod.module_name
    assert len(node.dependencies) == len(gomod.dependencies)
    for i, node_dep in enumerate(node.dependencies):
        node_dep == gomod.dependencies[i].ref_name


def test_parse_multiple_dependencies():
    gomod = parse_gomod(MULTIPLE_DEPENDENCIES_GOMOD)
    assert gomod.module_name == "example/hello"
    assert gomod.go_version == "1.17"
    assert len(gomod.dependencies) == 3
    deps = gomod.dependencies
    check_dependency(deps[0], "rsc.io/quote", "v1.5.2", "")
    check_dependency(
        deps[1], "golang.org/x/text", "v0.0.0-20170915032832-14c0d48ead0c", "indirect"
    )
    check_dependency(deps[2], "rsc.io/sampler", "v1.3.0", "indirect")


def test_convert_multiple_dependencies():
    gomod = parse_gomod(MULTIPLE_DEPENDENCIES_GOMOD)
    node = gomod_to_node(gomod)
    assert node.name == gomod.module_name
    assert len(node.dependencies) == len(gomod.dependencies)
    for i, node_dep in enumerate(node.dependencies):
        node_dep == gomod.dependencies[i].ref_name
