from gomoddepviz import Node, Graph


def test_empty_graph():
    g = Graph([])
    assert g.nodes == set()
    assert g.edges == {}


def test_single_node_no_dependency_graph():
    n = Node("a", [])
    g = Graph([n])
    assert g.nodes == {"a"}
    assert g.edges == {}


def test_single_node_with_dependencies_graph():
    n = Node("a", ["b", "c"])
    g = Graph([n])
    assert g.nodes == {"a", "b", "c"}
    assert g.edges == {"a": {"b", "c"}}


def test_single_node_with_dependencies_restricted_graph():
    n = Node("a", ["b", "c"])
    g = Graph([n], extended=False)
    assert g.nodes == {"a"}
    assert g.edges == {}


def test_two_nodes_graph():
    n1 = Node("a", ["b", "c"])
    n2 = Node("b", ["c"])
    g = Graph([n1, n2])
    assert g.nodes == {"a", "b", "c"}
    assert g.edges == {"a": {"b", "c"}, "b": {"c"}}


def test_two_nodes_restricted_graph():
    n1 = Node("a", ["b", "c"])
    n2 = Node("b", ["c"])
    g = Graph([n1, n2], extended=False)
    assert g.nodes == {"a", "b"}
    assert g.edges == {"a": {"b"}}
